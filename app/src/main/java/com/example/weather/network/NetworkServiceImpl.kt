package com.example.weather.network

import com.example.weather.model.WeatherResponse
import com.google.gson.Gson
import okhttp3.*
import java.io.IOException

class NetworkServiceImpl(val okHttpClient: OkHttpClient , val gson: Gson) : NetworkService {
    override fun getWeatherData(weatherListener: WeatherListener) {
        val builder = Request.Builder()
        builder.url("https://www.mocky.io/v2/5d3a99ed2f0000bac16ec13a")
        okHttpClient.newCall(builder.build()).enqueue(object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                weatherListener.onFailure()
            }

            override fun onResponse(call: Call, response: Response) {
                val data = gson.fromJson(response.body!!.string(),WeatherResponse::class.java)
                weatherListener.success(data = data)
            }

        })
    }
}