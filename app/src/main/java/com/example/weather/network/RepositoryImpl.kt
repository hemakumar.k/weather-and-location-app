package com.example.weather.network

class RepositoryImpl(val networkService: NetworkService) : Repository {
    override fun getWeatherData(weatherListener: WeatherListener) {
        networkService.getWeatherData(weatherListener)
    }

}