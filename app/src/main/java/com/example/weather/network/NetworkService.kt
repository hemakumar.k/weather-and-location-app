package com.example.weather.network

interface NetworkService {
    fun getWeatherData(weatherListener: WeatherListener)
}