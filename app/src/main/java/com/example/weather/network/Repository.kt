package com.example.weather.network

import com.example.weather.model.WeatherResponse

interface Repository {
    fun getWeatherData(weatherListener: WeatherListener)
}

interface WeatherListener{
    fun success(data : WeatherResponse)
    fun onFailure()
}