package com.example.weather.di

object AppComponentProvider {
    var appComponent : AppComponent? = null

    fun init(){
        if(appComponent == null){
            appComponent = DaggerAppComponent.builder().appModule(AppModule()).build()
        }
    }

    fun appComponent() :AppComponent {
        return appComponent
            ?:throw NullPointerException("Init not called.")
    }
}