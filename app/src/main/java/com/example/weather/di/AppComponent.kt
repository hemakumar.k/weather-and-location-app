package com.example.weather.di

import com.example.weather.network.NetworkService
import com.example.weather.network.Repository
import dagger.Component


@Component(modules = [AppModule::class])
@AppScope
interface AppComponent {
    fun provideRepository() : Repository
    fun provideNetworkService() : NetworkService
}