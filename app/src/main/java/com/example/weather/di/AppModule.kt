package com.example.weather.di

import com.example.weather.network.NetworkService
import com.example.weather.network.NetworkServiceImpl
import com.example.weather.network.Repository
import com.example.weather.network.RepositoryImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient


@Module
class AppModule() {

    @Provides
    @AppScope
    fun provideRepository(networkService: NetworkService) : Repository{
        return RepositoryImpl(networkService)
    }

    @Provides
    @AppScope
    fun provideOkHttpClient() : OkHttpClient{
        return OkHttpClient().newBuilder().build()
    }

    @Provides
    @AppScope
    fun providesGson() : Gson{
        return Gson()
    }

    @Provides
    @AppScope
    fun provideNetworkService(okHttpClient: OkHttpClient , gson: Gson) : NetworkService{
        return NetworkServiceImpl(okHttpClient,gson)
    }
}