package com.example.weather.di


@Retention(AnnotationRetention.RUNTIME)
@AppScope
annotation class AppScope