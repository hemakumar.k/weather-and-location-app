package com.example.weather

import android.app.Application
import com.example.weather.di.AppComponentProvider

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AppComponentProvider.init()
    }
}