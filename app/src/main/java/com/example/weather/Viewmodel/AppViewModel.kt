package com.example.weather.Viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weather.di.AppComponentProvider
import com.example.weather.model.WeatherResponseState
import com.example.weather.model.WeatherPermission
import com.example.weather.model.WeatherResponse
import com.example.weather.model.WeatherState
import com.example.weather.network.WeatherListener

class AppViewModel : ViewModel() {

    private val stateStream : MutableLiveData<WeatherState> = MutableLiveData()
    val state : LiveData<WeatherState> = stateStream



    init {
        stateStream.value = WeatherState()
    }


    fun updateLocationStatus(actionType : WeatherPermission){
        stateStream.value = state.value!!.copy(actionType)
    }

    fun fetchWeatherData(){
        val repository = AppComponentProvider.appComponent().provideRepository()
        stateStream.postValue(state.value!!.copy(weatherDate = WeatherResponseState.Loading))
        repository.getWeatherData(object  : WeatherListener{
            override fun success(data: WeatherResponse) {
                stateStream.postValue(state.value!!.copy(weatherDate = WeatherResponseState.Content(data)))
            }

            override fun onFailure() {
                stateStream.postValue(state.value!!.copy(weatherDate = WeatherResponseState.Error("Something Went Wrong")))
            }

        })
    }
}