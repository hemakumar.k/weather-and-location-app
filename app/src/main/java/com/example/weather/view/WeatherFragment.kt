package com.example.weather.view

import android.Manifest
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.R
import com.example.weather.Viewmodel.AppViewModel
import com.example.weather.model.WeatherResponseState
import com.example.weather.model.WeatherPermission
import com.example.weather.model.WeatherState

class WeatherFragment : Fragment(R.layout.weather_layout){

    private val REQUEST_CODE_LOCATION_PERMISSION = 1224

    private var locationManager : LocationManager? = null

    private val viewModel by lazy {
        ViewModelProviders.of(requireActivity()).get(AppViewModel::class.java)
    }


    override fun onStart() {
        super.onStart()
        if(!isLocationPermissionEnabled()){
            requestLocationPermission()
        }else{
            getCurrentLocation()
        }
        viewModel.fetchWeatherData()
        viewModel.state.observe(viewLifecycleOwner , Observer {
            renderWeatherData(it)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<AppCompatButton>(R.id.require_permission).setOnClickListener{
            requestLocationPermission()
        }
    }

    private fun renderWeatherData(weatherState : WeatherState){
        when (weatherState.weatherPermission) {
            WeatherPermission.PermissionDenied -> {
                requireView().findViewById<LinearLayout>(R.id.permission_denied_view).visibility = View.VISIBLE
                requireView().findViewById<LinearLayout>(R.id.latitude_longitude_view).visibility = View.GONE
            }

            is WeatherPermission.Location -> {
                val latitude = weatherState.weatherPermission.latitude.toInt()
                val longitude = weatherState.weatherPermission.longitude.toInt()
                requireView().findViewById<LinearLayout>(R.id.permission_denied_view).visibility = View.GONE
                requireView().findViewById<LinearLayout>(R.id.latitude_longitude_view).visibility = View.VISIBLE
                requireView().findViewById<TextView>(R.id.latitude_longitude_textView).text = String.format(requireContext().getString(R.string.location),latitude,longitude)
            }
        }
        when (weatherState.weatherDate) {
            is WeatherResponseState.Loading -> {
                requireView().findViewById<ProgressBar>(R.id.progress_bar).visibility = View.VISIBLE
                requireView().findViewById<RecyclerView>(R.id.weather_recycler_view)?.adapter = null
            }
            is WeatherResponseState.Content -> {
                requireView().findViewById<ProgressBar>(R.id.progress_bar).visibility = View.GONE
                if (requireView().findViewById<RecyclerView>(R.id.weather_recycler_view)?.adapter == null) {
                    requireView().findViewById<RecyclerView>(R.id.weather_recycler_view)?.apply {
                        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                        linearLayoutManager.orientation = RecyclerView.VERTICAL
                        layoutManager = linearLayoutManager
                        adapter = WeatherAdapter(weatherState.weatherDate.data.data)
                    }
                }
            }
            is WeatherResponseState.Error -> {
                requireView().findViewById<ProgressBar>(R.id.progress_bar).visibility = View.GONE
                requireView().findViewById<TextView>(R.id.error_text_view).visibility = View.VISIBLE
            }

            is WeatherResponseState.YetToFetch -> {
                requireView().findViewById<ProgressBar>(R.id.progress_bar).visibility = View.VISIBLE
            }
        }
    }

    private fun isLocationPermissionEnabled() : Boolean{
        return ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermission(){
        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION),REQUEST_CODE_LOCATION_PERMISSION)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_CODE_LOCATION_PERMISSION && grantResults.isNotEmpty()){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation()
            }else{
                viewModel.updateLocationStatus(WeatherPermission.PermissionDenied)
            }
        }
    }

    private fun getCurrentLocation() {
        try{
            locationManager = requireActivity().getSystemService(LOCATION_SERVICE) as LocationManager
            locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,5f) {
                viewModel.updateLocationStatus(WeatherPermission.Location(it.latitude , it.longitude))
            }
        }
        catch (e : SecurityException){
            e.printStackTrace()
        }
    }
}