package com.example.weather.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.R
import com.example.weather.model.DataItem
import java.sql.Date
import java.text.SimpleDateFormat

class WeatherAdapter(val data : List<DataItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =  LayoutInflater.from(parent.context).inflate(R.layout.weather_data, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        val holderData = data[position]
        holder.temp.text = holderData.temp+ " °C"
        val date = Date( holderData.time!!.toLong())
        val format = SimpleDateFormat("MMMM dd yyyy").format(date)
        holder.time.text = format
        holder.rain.text = holderData.rain + "%"
        holder.wind.text = holderData.wind + " Km/h"


    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val temp = view.findViewById<TextView>(R.id.temperature_text_view)
        val time = view.findViewById<TextView>(R.id.time_text_view)
        val rain = view.findViewById<TextView>(R.id.rain_text_view)
        val wind = view.findViewById<TextView>(R.id.wind_text_view)

    }

    override fun getItemCount(): Int {
        return data.size
    }

}