package com.example.weather.model

import com.google.gson.annotations.SerializedName

data class WeatherResponse(

	@field:SerializedName("data")
	val data: List<DataItem> = listOf()
)

data class DataItem(

	@field:SerializedName("rain")
	val rain: String? = null,

	@field:SerializedName("temp")
	val temp: String? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("wind")
	val wind: String? = null
)
