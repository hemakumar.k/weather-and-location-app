package com.example.weather.model

data class WeatherState(
    val weatherPermission: WeatherPermission  = WeatherPermission.Nothing,

    val weatherDate : WeatherResponseState<WeatherResponse> = WeatherResponseState.YetToFetch
)

sealed class WeatherResponseState<out T : Any>{
    object YetToFetch: WeatherResponseState<Nothing>()
    object Loading : WeatherResponseState<Nothing>()
    data class Content<out T : Any>(val data: T) : WeatherResponseState<T>()
    data class Error(val errorMessage: String) : WeatherResponseState<Nothing>()
}


sealed class WeatherPermission{
    object Nothing : WeatherPermission()

    object PermissionDenied : WeatherPermission()

    data class Location(val latitude : Double , val longitude : Double) : WeatherPermission()
}